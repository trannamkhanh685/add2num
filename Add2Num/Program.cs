﻿using Add2Num;
using Microsoft.Extensions.Logging;

//Log
using var loggerFactory = LoggerFactory.Create(builder =>
{
    builder
        .AddFilter("Microsoft", LogLevel.Warning)
        .AddFilter("System", LogLevel.Warning)
        .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
        .AddConsole();
});
ILogger logger = loggerFactory.CreateLogger<Program>();

string stn1, stn2;
MyBigNumber myBigNumber = new MyBigNumber();
Console.WriteLine("Nhap so thu nhat: ");
stn1 = Console.ReadLine();
Console.WriteLine("Nhap so thu hai: ");
stn2 = Console.ReadLine();
Console.WriteLine("Ket qua la: {0}",myBigNumber.sum(stn1, stn2));

logger.LogInformation("Ket qua cua {0} + {1}: {2}", stn1, stn2, myBigNumber.sum(stn1, stn2));