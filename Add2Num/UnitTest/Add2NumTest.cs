﻿
using NUnit.Framework;

namespace Add2Num.UnitTest
{
    public class Add2NumTest
    {
        [Test]
        public void TestNumber1()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("0", myBigNumber.sum("0", "0"));
        }
        [Test]
        public void TestNumber2()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("1", myBigNumber.sum("0", "1"));
        }
        [Test]
        public void TestNumber3()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("1", myBigNumber.sum("1", "0"));
        }
        [Test]
        public void TestNumber4()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("10", myBigNumber.sum("5", "5"));
        }
        [Test]
        public void TestNumber5()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("100", myBigNumber.sum("23", "77"));
        }
        [Test]
        public void TestNumber6()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("2131", myBigNumber.sum("1234", "897"));
        }
        [Test]
        public void TestNumber7()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("2131", myBigNumber.sum("897", "1234"));
        }
        [Test]
        public void TestNumber8()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("30", myBigNumber.sum("25", "5"));
        }
        [Test]
        public void TestNumber9()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("40", myBigNumber.sum("5", "35"));
        }
        [Test]
        public void TestNumber10()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("100000", myBigNumber.sum("99999", "1"));
        }
        [Test]
        public void TestNumber11()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
            Assert.AreEqual("1012218", myBigNumber.sum("24756", "987462"));
        }
    }
}
