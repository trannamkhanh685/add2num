﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Add2Num
{
    public class MyBigNumber
    {
        string sumRemainString(string remainString, int temp)
        {
            string result="";
            for(int i = remainString.Length-1; i >= 0; i--)
            {
                int x = int.Parse(remainString[i].ToString());
                if (temp == 1)
                {
                    if (temp + x >= 10)
                    {
                        result += ((x + temp) - 10).ToString();
                        temp = 1;
                    }
                    else
                    {
                        result += (x + temp).ToString();
                        temp = 0;
                    }
                }
                else result += (x).ToString();
            }
            if (temp == 1)
            {
                result += temp;
            }
            return result;
        }
        string reverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        public string sum(string stn1, string stn2)
        {
            string result="";
            int temp=0;
            if (stn1.Length == 1 && stn2.Length == 1)
            {
                return (int.Parse(stn1[0].ToString()) + int.Parse(stn2[0].ToString())).ToString();
            }
            else
            {
                int m = stn1.Length - 1;
                int n = stn2.Length - 1;
                while (m >= 0 && n >= 0)
                {
                    int x = int.Parse(stn1[m].ToString());
                    int y = int.Parse(stn2[n].ToString()); 
                    if (x + y + temp >= 10)
                    {
                        result += ((x + y + temp) - 10).ToString();
                        temp = 1;
                    }
                    else
                    {
                        temp = 0;
                        result += (x + y).ToString();
                    }
                    stn1 = stn1.Remove(m);
                    stn2 = stn2.Remove(n);
                    m--;
                    n--;
                }
                if (stn1 != "")
                {
                    result += sumRemainString(stn1, temp);
                }
                else if (stn2 != "") result += sumRemainString(stn2, temp);
                else if(temp==1) result += temp;
            }
            return reverseString(result);
        }
    }
}
